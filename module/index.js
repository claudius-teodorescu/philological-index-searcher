import jsUntar from "https://cdn.jsdelivr.net/npm/js-untar@2.0.0/+esm";
import init_searcher, { WasmStaticSearchEngine } from "./wasm_static_search_engine.js";
import init_oxigraph, * as oxigraph from "https://cdn.jsdelivr.net/npm/oxigraph@0.4.0-alpha.4/+esm";

await init_searcher();
await init_oxigraph();

export default class StaticSearchEngine {
    constructor() {
        this.metadata_store = new oxigraph.Store();
        this.index_metadata = [];
        this.default_index_url = "";
        this.index_data = {};
        this.locators_data = [];
        this.searcher = null;
    }

    async init(composite_index_url) {
        // load the composite index
        await fetch(composite_index_url, {
            mode: "cors",
            cache: "no-cache",
        })
            .then(response => response.blob())
            .then((blob) => {
                document.dispatchEvent(new CustomEvent("static-search-engine:index-size", {
                    "detail": this._sizeFormatter.format(blob.size),
                }));

                return blob;
            })
            .then(data => {
                let start = performance.now();
                let decompressionStream = data.stream().pipeThrough(new DecompressionStream('gzip'));
                let response = new Response(decompressionStream).arrayBuffer();
                let end = performance.now();
                document.dispatchEvent(new CustomEvent("static-search-engine:indexes-archive-decompressing-time", {
                    "detail": (end - start),
                }));

                return response;
            })
            .then(jsUntar)
            .then(async entries => {
                let start = performance.now();
                let filtered_entries = entries.filter(entry => entry.name !== "./");

                for (const filtered_entry of filtered_entries) {
                    let entry_name = filtered_entry.name;
                    let entry_type = filtered_entry.type;

                    switch (entry_type) {
                        // normal file
                        case "0":
                            let file_contents = new Uint8Array(await filtered_entry.blob.arrayBuffer());

                            switch (entry_name) {
                                case "metadata.ttl":
                                    this.#index_metadata_graph = new TextDecoder().decode(file_contents);
                                    break;
                                case "locators.fst":
                                    this.locators_data = file_contents;
                                    break;
                                default:
                                    // get the index position on index register
                                    let index_of_slash = entry_name.indexOf("/");
                                    let index_position = entry_name.substring(0, index_of_slash);
                                    let index_data_file_name = entry_name.substring(index_of_slash + 1);

                                    // create the structure for the index data, if doesn't exist
                                    if (!this.index_data.hasOwnProperty(index_position)) {
                                        this.index_data[index_position] = {};
                                    }

                                    // extract the headings and heading_ids_to_locators_ids.json for each index
                                    switch (index_data_file_name) {
                                        case "headings.fst":
                                            this.index_data[index_position]["headings"] = file_contents;
                                            break;
                                        case "heading_ids_to_locators_ids.fst":
                                            this.index_data[index_position]["heading_ids_to_locators_ids"] = file_contents;
                                            break;
                                    }
                            }
                            break;
                    }
                }
                let end = performance.now();
                document.dispatchEvent(new CustomEvent("static-search-engine:indexes-archive-extraction-time", {
                    "detail": (end - start),
                }));
            });

        // create the searcher
        let start = performance.now();
        this._process_index_metadata(this.#index_metadata_graph);
        let end = performance.now();
        document.dispatchEvent(new CustomEvent("static-search-engine:indexes-metadata-loading-time", {
            "detail": (end - start),
        }));

        start = performance.now();
        this.searcher = new WasmStaticSearchEngine(this.locators_data);

        this.index_metadata.forEach((_, i) => {
            let index_datum = this.index_data[i];
            this.searcher.set_index_data(i.toString(), index_datum.headings, index_datum.heading_ids_to_locators_ids);
        });
        //console.log(this.searcher.build());
        end = performance.now();
        document.dispatchEvent(new CustomEvent("static-search-engine:indexes-loading-time", {
            "detail": (end - start),
        }));
    }

    _process_index_metadata(index_metadata_graph) {
        this.metadata_store.load(index_metadata_graph, "text/turtle", null, oxigraph.Default);
        let aggregated_metadata_bindings = this.metadata_store.query(this.#aggregated_indexes_metadata);
        let index_metadata = [];
        this.default_index_url = "";

        // extract the metadata
        for (const binding of aggregated_metadata_bindings) {
            for (let [metadata_name, metadata_value] of binding.entries()) {
                if (metadata_name === "default_index_metadata_url") {
                    this.default_index_url = metadata_value.value;
                } else {
                    let [index_url, index_title, index_metadata_url] = metadata_value.value.split("=");
                    index_metadata.push({ index_url, index_title, index_metadata_url });
                }
            }
        }

        // process the character mappings for each index
        for (const index_metadatum of index_metadata) {
            this._process_character_mappings(index_metadatum);
        }

        this.index_metadata = index_metadata;
    }

    _process_character_mappings(index_metadata) {
        // extract the character normalization mappings
        let character_normalization_mappings = [];
        for (const binding of this.metadata_store.query(this._character_normalization_mappings(index_metadata.index_metadata_url))) {
            character_normalization_mappings.push(binding.get("character_normalisation_mapping").value);
        }

        // process the character normalization mappings, to obtain the search groups
        let search_groups = {};
        for (const character_normalization_mapping of character_normalization_mappings) {
            let [character, normalized_character] = character_normalization_mapping.split("=");

            if (search_groups.hasOwnProperty(normalized_character)) {
                search_groups[normalized_character] = `${search_groups[normalized_character]}|${character}`;
            } else {
                search_groups[normalized_character] = `${normalized_character}|${character}`;
            }
        }


        for (let [key, value] of Object.entries(search_groups)) {
            search_groups[key] = `[${value}]`;
        }
        index_metadata.search_groups = search_groups;
    }

    process_search_string(index_position, search_string) {
        let search_string_tokens = search_string
            .split("")
            .filter(Boolean);
        let processed_search_string_tokens = search_string_tokens
            .map(token => {
                let search_groups = this.index_metadata[index_position].search_groups;
                if (search_groups.hasOwnProperty(token)) {
                    return search_groups[token];
                } else {
                    return token;
                }
            });

        return `${processed_search_string_tokens.join("")}`;
    }

    #index_metadata_graph = "";

    #aggregated_indexes_metadata =
        `
        prefix i: <https://kuberam.ro/ontologies/text-index#>

        select ?default_index_metadata_url ?index_description
        where {
            ?default_index_metadata_url i:isDefaultIndex "true" .
            ?index_metadata_url i:metadataFor ?index_url .
            ?index_metadata_url i:title ?index_title .
            bind(concat(str(?index_url), '=', ?index_title, '=', str(?index_metadata_url)) as ?index_description)
        }
    `;

    _character_normalization_mappings(index_metadata_url) {
        return `
        prefix i: <https://kuberam.ro/ontologies/text-index#>

        select distinct ?character_normalisation_mapping
        where {
            <${index_metadata_url}> i:characterNormalisationMapping ?character_normalisation_mapping
        }
    `;
    }

    _sizeFormatter = new Intl.NumberFormat([], {
        style: 'unit',
        unit: 'byte',
        notation: "compact",
        unitDisplay: "narrow",
    });
};


