use std::collections::HashMap;

use console_error_panic_hook;
use serde_json;
use wasm_bindgen::prelude::*;

mod functions;

#[wasm_bindgen]
pub struct WasmStaticSearchEngine {
    locators: fst::raw::Fst<Vec<u8>>,
    index_data: HashMap<String, IndexData>,
}

#[wasm_bindgen]
impl WasmStaticSearchEngine {
    #[wasm_bindgen(constructor)]
    pub fn new(locators_data: Vec<u8>) -> WasmStaticSearchEngine {
        console_error_panic_hook::set_once();

        let index_data: HashMap<String, IndexData> = HashMap::new();

        let locators = fst::Map::new(locators_data).unwrap().as_fst().to_owned();

        WasmStaticSearchEngine {
            locators,
            index_data,
        }
    }

    pub fn set_index_data(
        &mut self,
        index_position: &str,
        headings_data: Vec<u8>,
        heading_ids_to_locators_ids_data: Vec<u8>,
    ) {
        let headings = fst::Map::new(headings_data).unwrap();
        let heading_ids_to_locators_ids = fst::Set::new(heading_ids_to_locators_ids_data).unwrap();

        self.index_data.insert(
            index_position.to_string(),
            IndexData {
                headings,
                heading_ids_to_locators_ids,
            },
        );
    }

    pub fn build(&mut self) -> Result<js_sys::Array, JsError> {
        // build the locator-all_heading_ids calculated index
        let results = crate::functions::generate_locator_all_heading_ids_index(self);

        Ok(results.into_iter().map(JsValue::from).collect())
    }

    pub fn headings_by_heading_regex(
        &mut self,
        index_position: &str,
        heading_string: &str,
    ) -> Result<JsValue, JsValue> {
        let results: HashMap<String, String> =
            crate::functions::headings_by_heading_regex(self, index_position, heading_string);

        Ok(serde_wasm_bindgen::to_value(&results)?)
    }

    pub fn locators_by_heading_id(
        &mut self,
        index_position: &str,
        heading_id: &str,
    ) -> Result<JsValue, JsValue> {
        let results: HashMap<String, String> =
            crate::functions::locators_by_heading_id(self, index_position, heading_id);

        Ok(serde_wasm_bindgen::to_value(&results)?)
    }

    pub fn locator_ids_by_heading_regex() {}

    pub fn headings_by_heading_levenstein_1(
        &mut self,
        index_position: &str,
        heading_string: &str,
        distance: &str,
    ) -> Result<JsValue, JsValue> {
        let results: HashMap<String, String> = crate::functions::headings_by_heading_levenstein(
            self,
            index_position,
            heading_string,
            distance,
        );

        Ok(serde_wasm_bindgen::to_value(&results)?)
    }

    pub fn associated_headings_by_locators(
        &mut self,
        locators: js_sys::Map,
        other_index_positions: Vec<String>,
    ) -> js_sys::Array {
        if locators.size() > 0 {
            let results: js_sys::Array = crate::functions::associated_headings_by_locators(
                self,
                locators,
                other_index_positions,
            );

            results
        } else {
            js_sys::Array::new()
        }
    }

    /*


    pub fn levenstein_2_search(&mut self, query: &str) -> Result<js_sys::Map, JsError> {
        let levenstein = automaton::Levenshtein::new(query, 2)?;

        let stream = &mut self.map.search(levenstein).into_stream();

        let results = js_sys::Map::new();
        while let Some((k, v)) = stream.next() {
            results.set(
                &wasm_bindgen::JsValue::from(std::str::from_utf8(k).unwrap()),
                &wasm_bindgen::JsValue::from(v.to_string()),
            );
        }

        Ok(results)
    }
    */
}

struct IndexData {
    headings: fst::Map<Vec<u8>>,
    heading_ids_to_locators_ids: fst::Set<Vec<u8>>,
}
