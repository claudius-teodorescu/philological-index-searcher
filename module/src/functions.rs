use crate::WasmStaticSearchEngine;
use fst::{
    automaton::{self, Str},
    set, Automaton, IntoStreamer, Set, Streamer,
};
use js_sys;
use regex_automata::dense;
use std::collections::{BTreeMap, BTreeSet, HashMap};

static SUFFIX_DELIMITER: u8 = 45;
static SUFFIX_DELIMITER_AS_CHAR: char = SUFFIX_DELIMITER as char;

// locator_id␝h_1␟h_2␞h3␟h_4
pub fn generate_locator_all_heading_ids_index(
    index_search_engine: &mut WasmStaticSearchEngine,
) -> Vec<String> {
    let mut locators_stream = index_search_engine.locators.into_stream();
    let locator_ids_count = index_search_engine.locators.len();

    let mut locator_all_headings_ids: Vec<String> = Vec::with_capacity(locator_ids_count);

    // loop over the indexes, in order to gather data about the heading_id to locator_id mappings
    for (_, index_data) in index_search_engine.index_data.iter() {
        let mut stream = index_data.heading_ids_to_locators_ids.stream();

        while let Some(key) = stream.next() {
            locator_all_headings_ids.push(std::str::from_utf8(key).unwrap().to_string());
        }
    }

    locator_all_headings_ids
}

pub fn headings_by_heading_regex(
    index_search_engine: &mut WasmStaticSearchEngine,
    index_position: &str,
    heading_string: &str,
) -> HashMap<String, String> {
    let formatted_query = format!(r"(?i){}", heading_string);
    let dfa = dense::Builder::new()
        .anchored(true)
        .build(formatted_query.as_str())
        .unwrap();

    let stream = &mut index_search_engine
        .index_data
        .get(index_position)
        .unwrap()
        .headings
        .search(&dfa)
        .into_stream();

    let mut results: HashMap<String, String> = HashMap::new();
    while let Some((key, value)) = stream.next() {
        results.insert(
            std::str::from_utf8(key).unwrap().to_string(),
            value.to_string(),
        );
    }

    results
}

pub fn headings_by_heading_levenstein(
    index_search_engine: &mut crate::WasmStaticSearchEngine,
    index_position: &str,
    heading_string: &str,
    distance: &str,
) -> HashMap<String, String> {
    let levenstein =
        automaton::Levenshtein::new(heading_string, distance.parse::<u32>().unwrap()).unwrap();

    let stream = &mut index_search_engine
        .index_data
        .get(index_position)
        .unwrap()
        .headings
        .search(levenstein)
        .into_stream();

    let mut headings: HashMap<String, String> = HashMap::new();
    while let Some((key, value)) = stream.next() {
        headings.insert(
            std::str::from_utf8(key).unwrap().to_string(),
            value.to_string(),
        );
    }

    headings
}

pub fn locators_by_heading_id(
    index_search_engine: &mut crate::WasmStaticSearchEngine,
    index_position: &str,
    heading_id: &str,
) -> HashMap<String, String> {
    let heading_id_with_delimiter = format!("{}{}", heading_id, SUFFIX_DELIMITER_AS_CHAR);
    let heading_query_string = Str::new(heading_id_with_delimiter.as_str()).starts_with();

    let mut locator_ids_stream = index_search_engine
        .index_data
        .get(index_position)
        .unwrap()
        .heading_ids_to_locators_ids
        .search(heading_query_string)
        .into_stream();

    let mut locators: HashMap<String, String> = HashMap::new();
    while let Some(key) = locator_ids_stream.next() {
        let (locator_id, locator_iri) = _get_locator(key, index_search_engine);

        locators.insert(locator_iri.to_string(), locator_id.to_string());
    }

    locators
}

pub fn associated_headings_by_locators(
    index_search_engine: &mut crate::WasmStaticSearchEngine,
    locators: js_sys::Map,
    other_index_positions: Vec<String>,
) -> js_sys::Array {
    let results: js_sys::Object = js_sys::Object::new();
    let results2: js_sys::Array = js_sys::Array::new();

    // loop over the locators
    locators.for_each(&mut |value, key| {
        let locator_iri = key.as_string().unwrap();
        let locator_id = value.as_string().unwrap();

        // loop over the array with the other index positions, to get the respective headers
        let other_headers_container = js_sys::Array::new();
        for other_index_position in other_index_positions.iter() {
            let other_index_data = index_search_engine
                .index_data
                .get(other_index_position)
                .unwrap();

            // get header id-s by locator id
            let formatted_query = format!(r".*{}{}", SUFFIX_DELIMITER_AS_CHAR, locator_id);
            let dfa = dense::Builder::new()
                .anchored(true)
                .build(formatted_query.as_str())
                .unwrap();

            let mut heading_ids_to_locators_ids_stream = other_index_data
                .heading_ids_to_locators_ids
                .search(&dfa)
                .into_stream();

            // get the headings
            let headings: js_sys::Array = js_sys::Array::new();
            while let Some(key) = heading_ids_to_locators_ids_stream.next() {
                let key_splitted: Vec<_> = key.split(|i| *i == SUFFIX_DELIMITER).collect();
                let heading_id_bytes = *key_splitted.get(0).unwrap();
                let heading_id_str = std::str::from_utf8(heading_id_bytes).unwrap();
                /*let heading_id: u64 = heading_id_str.parse::<u64>().unwrap();

                let heading_raw = &other_index_data
                    .headings
                    .as_fst()
                    .get_key(heading_id)
                    .unwrap();
                let heading_value = std::str::from_utf8(heading_raw).unwrap();

                headings.push(&heading_value.into());*/
            }

            /*let _ = js_sys::Reflect::set(
                &other_headers_container,
                &other_index_position.into(),
                &headings,
            );*/
            other_headers_container.push(&headings);
        }

        let _ = js_sys::Reflect::set(&results, &locator_iri.into(), &other_headers_container);
        results2.push(&other_headers_container);
    });

    results2
}
/*
pub fn headings_by_locator_ids(
    index_search_engine: &mut crate::WasmStaticSearchEngine,
    index_position: &str,
    heading_id: &str,
    locator_ids: Vec<String>,
    other_index_positions: Vec<String>,
) -> js_sys::Object {
    let heading_id_with_delimiter = format!("{}{}", heading_id, SUFFIX_DELIMITER_AS_CHAR);
    let heading_query_string = Str::new(heading_id_with_delimiter.as_str()).starts_with();

    let mut locator_ids_stream = index_search_engine
        .index_data
        .get(index_position)
        .unwrap()
        .heading_ids_to_locators_ids
        .search(heading_query_string)
        .into_stream();

    let results: js_sys::Object = js_sys::Object::new();
    for locator_id in locator_ids.iter() {
        let (locator_id, locator_iri) = _get_locator(key, index_search_engine);

        // loop over the array with the other index positions, to get the respective headers
        let other_headers_container = js_sys::Object::new();
        for other_index_position in other_index_positions.iter() {
            let other_index_data = index_search_engine
                .index_data
                .get(other_index_position)
                .unwrap();

            // get header id-s by locator id
            let formatted_query = format!(r".*{}{}", SUFFIX_DELIMITER_AS_CHAR, locator_id);
            let dfa = dense::Builder::new()
                .anchored(true)
                .build(formatted_query.as_str())
                .unwrap();

            let mut heading_ids_to_locators_ids_stream = other_index_data
                .heading_ids_to_locators_ids
                .search(&dfa)
                .into_stream();

            // get the headings
            let headings: js_sys::Array = js_sys::Array::new();
            while let Some(key) = heading_ids_to_locators_ids_stream.next() {
                let key_splitted: Vec<_> = key.split(|i| *i == SUFFIX_DELIMITER).collect();
                let heading_id_bytes = *key_splitted.get(0).unwrap();
                let heading_id_str = std::str::from_utf8(heading_id_bytes).unwrap();
                let heading_id: u64 = heading_id_str.parse::<u64>().unwrap();

                let heading_raw = &other_index_data
                    .headings
                    .as_fst()
                    .get_key(heading_id)
                    .unwrap();
                let heading_value = std::str::from_utf8(heading_raw).unwrap();

                headings.push(&heading_value.into());
            }

            let _ = js_sys::Reflect::set(
                &other_headers_container,
                &other_index_position.into(),
                &headings,
            );
        }

        let _ = js_sys::Reflect::set(&results, &locator_iri.into(), &other_headers_container);
    }

    results
}
*/
fn _get_locator(key: &[u8], index_search_engine: &WasmStaticSearchEngine) -> (u64, String) {
    // get the locator id
    let key_splitted: Vec<_> = key.split(|i| *i == SUFFIX_DELIMITER).collect();
    let key_suffix = *key_splitted.get(1).unwrap();
    let key_suffix_str = std::str::from_utf8(key_suffix).unwrap();
    let locator_id: u64 = key_suffix_str.parse::<u64>().unwrap();

    //get the locator IRI
    let locator_raw = &index_search_engine.locators.get_key(locator_id).unwrap();
    let locator_iri = std::str::from_utf8(locator_raw).unwrap();

    (locator_id, locator_iri.to_string())
}

trait IntersectionByPrefixExt<'s> {
    fn intersection_by_prefix<'a>(self, suffix_delimiter: u8) -> IntersectionByPrefix<'a>
    where
        's: 'a;
}

impl<'b> IntersectionByPrefixExt<'b> for set::OpBuilder<'b> {
    #[inline]
    fn intersection_by_prefix<'a>(self, suffix_delimiter: u8) -> IntersectionByPrefix<'a>
    where
        'b: 'a,
    {
        IntersectionByPrefix(self.0.intersection_by_prefix(suffix_delimiter))
    }
}

trait RawIntersectionByPrefixExt<'s> {
    fn intersection_by_prefix<'a>(self, suffix_delimiter: u8) -> RawIntersectionByPrefix<'a>
    where
        's: 'a;
}

impl<'b> RawIntersectionByPrefixExt<'b> for fst::raw::OpBuilder<'b> {
    #[inline]
    fn intersection_by_prefix<'a>(self, suffix_delimiter: u8) -> RawIntersectionByPrefix<'a>
    where
        'b: 'a,
    {
        RawIntersectionByPrefix {
            heap: StreamHeap::new(self.streams, suffix_delimiter),
            outs: vec![],
            cur_slot: None,
            suffix_delimiter,
        }
    }
}

pub struct IntersectionByPrefix<'s>(RawIntersectionByPrefix<'s>);

impl<'a, 's> Streamer<'a> for IntersectionByPrefix<'s> {
    type Item = &'a [u8];

    #[inline]
    fn next(&'a mut self) -> Option<&'a [u8]> {
        self.0.next().map(|(key, _)| key)
    }
}

pub struct RawIntersectionByPrefix<'f> {
    heap: StreamHeap<'f>,
    outs: Vec<fst::raw::IndexedValue>,
    cur_slot: Option<Slot>,
    suffix_delimiter: u8,
}

impl<'a, 'f> Streamer<'a> for RawIntersectionByPrefix<'f> {
    type Item = (&'a [u8], &'a [fst::raw::IndexedValue]);

    fn next(&'a mut self) -> Option<Self::Item> {
        if let Some(slot) = self.cur_slot.take() {
            self.heap.refill(slot);
        }
        loop {
            let slot = match self.heap.pop() {
                None => return None,
                Some(slot) => {
                    let slot_input = slot.input;
                    let slot_input_splitted: Vec<_> =
                        slot_input.split(|i| *i == self.suffix_delimiter).collect();
                    let slot_input_suffix = *slot_input_splitted.get(1).unwrap();

                    let mut slot_by_suffix = Slot::new(slot.idx);
                    slot_by_suffix.set_input(slot_input_suffix);
                    slot_by_suffix.set_output(slot.output);

                    slot_by_suffix
                }
            };
            self.outs.clear();
            self.outs.push(slot.indexed_value());
            let mut popped: usize = 1;
            while let Some(slot2) = self.heap.pop_if_equal_by_suffix(slot.input()) {
                self.outs.push(slot2.indexed_value());
                self.heap.refill(slot2);
                popped += 1;
            }
            if popped < self.heap.num_slots() {
                self.heap.refill(slot);
            } else {
                self.cur_slot = Some(slot);
                let key = self.cur_slot.as_ref().unwrap().input();
                return Some((key, &self.outs));
            }
        }
    }
}

type BoxedStream<'f> = Box<dyn for<'a> Streamer<'a, Item = (&'a [u8], fst::raw::Output)> + 'f>;

struct StreamHeap<'f> {
    rdrs: Vec<BoxedStream<'f>>,
    heap: std::collections::BinaryHeap<Slot>,
    suffix_delimiter: u8,
}

impl<'f> StreamHeap<'f> {
    fn new(streams: Vec<BoxedStream<'f>>, suffix_delimiter: u8) -> StreamHeap<'f> {
        let mut u = StreamHeap {
            rdrs: streams,
            heap: std::collections::BinaryHeap::new(),
            suffix_delimiter,
        };
        for i in 0..u.rdrs.len() {
            u.refill(Slot::new(i));
        }
        u
    }

    fn pop(&mut self) -> Option<Slot> {
        self.heap.pop()
    }

    fn peek_is_duplicate_by_suffix(&self, key: &[u8]) -> bool {
        self.heap
            .peek()
            .map(|s| {
                let slot_input_splitted: Vec<_> =
                    s.input().split(|i| *i == self.suffix_delimiter).collect();
                let slot_input_suffix = *slot_input_splitted.get(1).unwrap();

                slot_input_suffix == key
            })
            .unwrap_or(false)
    }

    fn pop_if_equal_by_suffix(&mut self, key: &[u8]) -> Option<Slot> {
        if self.peek_is_duplicate_by_suffix(key) {
            self.pop()
        } else {
            None
        }
    }

    fn num_slots(&self) -> usize {
        self.rdrs.len()
    }

    fn refill(&mut self, mut slot: Slot) {
        if let Some((input, output)) = self.rdrs[slot.idx].next() {
            slot.set_input(input);
            slot.set_output(output);
            self.heap.push(slot);
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
struct Slot {
    idx: usize,
    input: Vec<u8>,
    output: fst::raw::Output,
}

impl Slot {
    fn new(rdr_idx: usize) -> Slot {
        Slot {
            idx: rdr_idx,
            input: Vec::with_capacity(64),
            output: fst::raw::Output::zero(),
        }
    }

    fn indexed_value(&self) -> fst::raw::IndexedValue {
        fst::raw::IndexedValue {
            index: self.idx,
            value: self.output.value(),
        }
    }

    fn input(&self) -> &[u8] {
        &self.input
    }

    fn set_input(&mut self, input: &[u8]) {
        self.input.clear();
        self.input.extend(input);
    }

    fn set_output(&mut self, output: fst::raw::Output) {
        self.output = output;
    }
}

impl PartialOrd for Slot {
    fn partial_cmp(&self, other: &Slot) -> Option<std::cmp::Ordering> {
        (&self.input, self.output)
            .partial_cmp(&(&other.input, other.output))
            .map(|ord| ord.reverse())
    }
}

impl Ord for Slot {
    fn cmp(&self, other: &Slot) -> std::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

#[test]
fn fst_intersection_1_test() {
    use std::time::Instant;

    let set_1 = Set::from_iter(vec!["1-1", "1-2", "1-3"]).unwrap();
    let set_2 = Set::from_iter(vec!["2-2", "2-4"]).unwrap();

    let now = Instant::now();
    let mut stream = set::OpBuilder::new()
        .add(set_1.into_stream())
        .add(set_2.into_stream())
        .intersection_by_prefix(45);
    let elapsed = now.elapsed();
    println!("Time for intersection: {:.2?}", elapsed);

    let mut keys = vec![];
    while let Some(key) = stream.next() {
        keys.push(String::from_utf8(key.to_vec()).unwrap());
    }
    println!("{:?}", keys);
}

#[test]
fn fst_intersection_2_test() {
    use fst::Set;
    use memmap::Mmap;
    use std::fs::File;
    use std::time::Instant;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/index-search-engine/tests/data/".to_string();

    let locators_data = unsafe {
        Mmap::map(&File::open(index_dir_path.clone() + "heading_ids_to_locators_ids.fst").unwrap())
            .unwrap()
    };
    let set = Set::new(locators_data).unwrap();

    // get the first stream
    let query_1 = Str::new("1-").starts_with();
    let stream_1 = set.search(query_1).into_stream();

    let query_2 = Str::new("2-").starts_with();
    let stream_2 = set.search(query_2).into_stream();

    let now = Instant::now();
    let mut stream = set::OpBuilder::new()
        .add(stream_1)
        .add(stream_2)
        .intersection_by_prefix(45);
    let elapsed = now.elapsed();
    println!("Time for intersection: {:.2?}", elapsed);

    let mut keys = vec![];
    while let Some(key) = stream.next() {
        keys.push(String::from_utf8(key.to_vec()).unwrap());
    }
    println!("{:?}", keys.len());
}

#[test]
fn into_strs_test() {
    use fst::Map;
    use memmap::Mmap;
    use std::fs::File;
    use std::time::Instant;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/index-search-engine/tests/data/".to_string();

    let heading_ids_to_locators_ids_data = unsafe {
        Mmap::map(&File::open(index_dir_path.clone() + "heading_ids_to_locators_ids.fst").unwrap())
            .unwrap()
    };
    let heading_ids_to_locators_ids_ds = Set::new(heading_ids_to_locators_ids_data).unwrap();

    let locators_data = unsafe {
        Mmap::map(&File::open(index_dir_path.clone() + "locators.fst").unwrap()).unwrap()
    };
    let locators_map = Map::new(locators_data).unwrap();
    let locators_ds = locators_map.as_fst();

    // get the first stream
    let query_1 = Str::new("155-").starts_with();
    let stream_1 = heading_ids_to_locators_ids_ds.search(query_1).into_stream();
    let mut locators: Vec<String> = Vec::new();
    let mut now = Instant::now();
    let heading_id_to_locators_ids = stream_1.into_strs().unwrap();
    for heading_id_to_locators_id in heading_id_to_locators_ids {
        let key_splitted: Vec<_> = heading_id_to_locators_id
            .as_bytes()
            .split(|i| *i == SUFFIX_DELIMITER)
            .collect();
        let key_suffix = *key_splitted.get(1).unwrap();
        let key_suffix_str = std::str::from_utf8(key_suffix).unwrap();
        let locator_id: u64 = key_suffix_str.parse::<u64>().unwrap();
        let locator_raw = &mut locators_ds.get_key(locator_id).unwrap();
        let locator = std::string::String::from_utf8(locator_raw.to_vec()).unwrap();

        locators.push(locator);
    }
    let mut elapsed = now.elapsed();
    println!("Time for into_strs(): {:.2?}", elapsed);
    println!("{:?}", locators.len());

    let query_2 = Str::new("155-").starts_with();
    let mut stream_2 = heading_ids_to_locators_ids_ds.search(query_2).into_stream();
    let mut locators: Vec<String> = Vec::new();
    now = Instant::now();
    while let Some(key) = stream_2.next() {
        let key_splitted: Vec<_> = key.split(|i| *i == SUFFIX_DELIMITER).collect();
        let key_suffix = *key_splitted.get(1).unwrap();
        let key_suffix_str = std::str::from_utf8(key_suffix).unwrap();
        let locator_id: u64 = key_suffix_str.parse::<u64>().unwrap();
        let locator_raw = &mut locators_ds.get_key(locator_id).unwrap();
        let locator = std::string::String::from_utf8(locator_raw.to_vec()).unwrap();

        locators.push(locator);
    }
    elapsed = now.elapsed();
    println!("Time for locators_ds.get_key(locator_id): {:.2?}", elapsed);
    println!("{:?}", locators);
}

#[test]
fn locators_by_heading_id_test() {
    use fst::Map;
    use memmap::Mmap;
    use std::fs::File;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/index-search-engine/tests/data/".to_string();

    let locators_data = unsafe {
        Mmap::map(&File::open(index_dir_path.clone() + "locators.fst").unwrap()).unwrap()
    };
    let locators_map = Map::new(locators_data).unwrap();
    let locators_ds = locators_map.as_fst();

    let heading_id = "154";

    let heading_id_with_delimiter = format!("{}{}", heading_id, SUFFIX_DELIMITER_AS_CHAR);
    let heading_query_string = Str::new(heading_id_with_delimiter.as_str()).starts_with();

    let heading_ids_to_locators_ids_data = unsafe {
        Mmap::map(&File::open(index_dir_path.clone() + "heading_ids_to_locators_ids.fst").unwrap())
            .unwrap()
    };
    let heading_ids_to_locators_ids_ds = Set::new(heading_ids_to_locators_ids_data).unwrap();

    let mut locator_ids_stream = heading_ids_to_locators_ids_ds
        .search(heading_query_string)
        .into_stream();

    let mut locators: Vec<String> = Vec::new();
    while let Some(key) = locator_ids_stream.next() {
        let key_splitted: Vec<_> = key.split(|i| *i == SUFFIX_DELIMITER).collect();
        let key_suffix = *key_splitted.get(1).unwrap();
        let key_suffix_str = std::str::from_utf8(key_suffix).unwrap();
        let locator_id: u64 = key_suffix_str.parse::<u64>().unwrap();
        let locator_raw = &mut locators_ds.get_key(locator_id).unwrap();
        let locator = std::string::String::from_utf8(locator_raw.to_vec()).unwrap();

        locators.push(locator);
    }

    println!("{:?}", locators);
}

#[test]
fn locators_and_other_headings_by_heading_id_test() {
    use fst::Map;
    use memmap::Mmap;
    use std::{fs::File, io};

    let base_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/examples/dwds-data/public/peritext/indexes/_heading_ids_to_locators_ids/".to_string();
    let mut keys: BTreeSet<String> = BTreeSet::new();

    // load the first hl file
    let hl_1_data = unsafe {
        Mmap::map(&File::open(base_path.clone() + "0/heading_ids_to_locators_ids.fst").unwrap())
            .unwrap()
    };
    let hl_1_map = Map::new(hl_1_data).unwrap();

    let heading_id = "17";
    let heading_id_with_delimiter = format!("{}{}", heading_id, SUFFIX_DELIMITER_AS_CHAR);
    let heading_query_string = Str::new(heading_id_with_delimiter.as_str()).starts_with();

    // get the locator ids from the base index

    let mut locator_ids_stream = hl_1_map.search(heading_query_string).into_stream();

    let mut result: HashMap<String, String> = HashMap::new();
    /*while let Some(key) = locator_ids_stream.next() {
        let key_splitted: Vec<_> = key.split(|i| *i == SUFFIX_DELIMITER).collect();
        let key_suffix = *key_splitted.get(1).unwrap();
        let key_suffix_str = std::str::from_utf8(key_suffix).unwrap();
        let locator_id: u64 = key_suffix_str.parse::<u64>().unwrap();
        let locator_raw = &mut index_search_engine.locators.get_key(locator_id).unwrap();
        let locator = std::string::String::from_utf8(locator_raw.to_vec()).unwrap();

        result.push(locator);
    }*/
}
