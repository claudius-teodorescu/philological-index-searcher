#[test]
fn fst_size_test() {
    use fst::SetBuilder;
    use std::{fs, io};

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/index-search-engine/tests/data/".to_string();

    let heading_ids_to_locators_ids_data =
        fs::read(index_dir_path.clone() + "heading_ids_to_locators_ids.json").unwrap();
    let heading_ids_to_locators_ids: Vec<String> = serde_json_wasm::de::from_str(
        std::str::from_utf8(&heading_ids_to_locators_ids_data).unwrap(),
    )
    .expect("err: unable to parse json");

    let mut array: Vec<&str> = heading_ids_to_locators_ids
        .get(1)
        .unwrap()
        .split("␟")
        .collect();
    array.sort();
    println!("{:?}", array.len());

    let fst_file_handle = std::fs::File::create(index_dir_path.clone() + "fst_size_test.fst")
        .expect("Cannot create the FST file.");
    let fst_buffered_writer = io::BufWriter::new(fst_file_handle);

    let mut fst_builder = SetBuilder::new(fst_buffered_writer).expect("Generate map builder.");

    for value in array.iter() {
        fst_builder.insert(value).unwrap();
    }

    fst_builder
        .finish()
        .expect("Finish generation of the FST map.");
}

#[test]
fn roaring_size_test() {
    use std::fs;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/index-search-engine/tests/data/".to_string();

    let heading_ids_to_locators_ids_data =
        fs::read(index_dir_path.clone() + "heading_ids_to_locators_ids.json").unwrap();
    let heading_ids_to_locators_ids: Vec<String> = serde_json_wasm::de::from_str(
        std::str::from_utf8(&heading_ids_to_locators_ids_data).unwrap(),
    )
    .expect("err: unable to parse json");

    let rb_1_array: Vec<&str> = heading_ids_to_locators_ids
        .get(1)
        .unwrap()
        .split("␟")
        .collect();
    let mut rb_1 = RoaringBitmap::new();

    for item in rb_1_array {
        rb_1.insert(item.parse::<u32>().unwrap());
    }

    let json = serde_json::to_vec(&rb_1).unwrap();

    fs::write(
        index_dir_path.clone() + "roaring_size_test.bincode",
        json,
    )
    .expect("cannot write the file");
}

#[test]
fn roaring_intersection_test() {
    use std::fs;
    use std::time::Instant;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/index-search-engine/tests/data/".to_string();

    let heading_ids_to_locators_ids_data =
        fs::read(index_dir_path.clone() + "heading_ids_to_locators_ids.json").unwrap();
    let heading_ids_to_locators_ids: Vec<String> = serde_json_wasm::de::from_str(
        std::str::from_utf8(&heading_ids_to_locators_ids_data).unwrap(),
    )
    .expect("err: unable to parse json");

    let rb_1_array: Vec<&str> = heading_ids_to_locators_ids
        .get(1)
        .unwrap()
        .split("␟")
        .collect();
    let mut rb_1 = RoaringBitmap::new();

    for item in rb_1_array {
        rb_1.insert(item.parse::<u32>().unwrap());
    }

    let rb_2_array: Vec<&str> = heading_ids_to_locators_ids
        .get(2)
        .unwrap()
        .split("␟")
        .collect();
    let mut rb_2 = RoaringBitmap::new();

    for item in rb_2_array {
        rb_2.insert(item.parse::<u32>().unwrap());
    }

    let bitmaps = [&rb_1, &rb_2];

    let now = Instant::now();
    let intersection_bitmap = bitmaps.intersection();
    let elapsed = now.elapsed();
    println!("Time for intesection with roaring bitmap: {:.2?}", elapsed);

    println!("{:?}", intersection_bitmap);
}

#[test]
fn fst_intersection_test() {
    use fst::{set, Set};
    use std::fs;
    use std::time::Instant;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/index-search-engine/tests/data/".to_string();

    let heading_ids_to_locators_ids_data =
        fs::read(index_dir_path.clone() + "heading_ids_to_locators_ids.json").unwrap();
    let heading_ids_to_locators_ids: Vec<String> = serde_json_wasm::de::from_str(
        std::str::from_utf8(&heading_ids_to_locators_ids_data).unwrap(),
    )
    .expect("err: unable to parse json");

    let array_1: Vec<&str> = heading_ids_to_locators_ids
        .get(1)
        .unwrap()
        .split("␟")
        .collect();
    let set_1 = Set::from_iter(array_1).unwrap();

    let array_2: Vec<&str> = heading_ids_to_locators_ids
        .get(2)
        .unwrap()
        .split("␟")
        .collect();
    let set_2 = Set::from_iter(array_2).unwrap();

    let now = Instant::now();
    let mut stream = set::OpBuilder::new()
        .add(set_1.into_stream())
        .add(set_2.into_stream())
        .intersection();
    let elapsed = now.elapsed();
    println!("Time for intersection: {:.2?}", elapsed);

    let mut keys = vec![];
    while let Some(key) = stream.next() {
        keys.push(String::from_utf8(key.to_vec()).unwrap());
    }
    println!("{:?}", keys.len());
}

#[test]
fn prefix_search_test() {
    use fst::automaton::Str;
    use fst::{IntoStreamer, Set};
    use memmap::Mmap;
    use std::fs::File;
    use std::time::Instant;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/_aggregated/".to_string();

    // test for FST range
    let locators_data = unsafe {
        Mmap::map(&File::open(index_dir_path.clone() + "new-locators.fst").unwrap()).unwrap()
    };
    let mut set = Set::new(locators_data).unwrap();

    // search with range()
    let mut now = Instant::now();
    let mut stream_range = set.range().ge("1-").le("10").into_stream();
    let mut elapsed = now.elapsed();
    println!("Time for retrieving a value with range(): {:.2?}", elapsed);
    let mut keys_range = vec![];
    while let Some(key_bytes) = stream_range.next() {
        keys_range.push(std::str::from_utf8(key_bytes).unwrap().to_string());
    }
    println!("{:?}", keys_range.len());

    // search with starts_with()
    now = Instant::now();
    let starts_with_query = Str::new("1-").starts_with();
    let mut stream_starts_with = set.search(starts_with_query).into_stream();
    elapsed = now.elapsed();
    println!(
        "Time for retrieving a value with starts_with(): {:.2?}",
        elapsed
    );
    let mut keys_starts_with = vec![];
    while let Some(key_bytes) = stream_starts_with.next() {
        keys_starts_with.push(std::str::from_utf8(key_bytes).unwrap().to_string());
    }
    println!("{:?}", keys_starts_with.len());

    // search with DFA
    now = Instant::now();
    let dfa = dense::Builder::new().anchored(true).build("1-.*").unwrap();
    let mut stream_dfa = set.search(&dfa).into_stream();
    elapsed = now.elapsed();
    println!("Time for retrieving a value with DFA: {:.2?}", elapsed);
    let mut keys_dfa = vec![];
    while let Some(key_bytes) = stream_dfa.next() {
        keys_dfa.push(std::str::from_utf8(key_bytes).unwrap().to_string());
    }
    println!("{:?}", keys_dfa.len());
}

#[test]
fn intersection_of_ranges_test2() {
    use fst::{IntoStreamer, Set, SetBuilder, Streamer};
    use memmap::Mmap;
    use std::time::Instant;
    use std::{fs, fs::File, io};

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/_aggregated/".to_string();

    // test for FST range
    let locators_data = unsafe {
        Mmap::map(&File::open(index_dir_path.clone() + "new-locators.fst").unwrap()).unwrap()
    };
    let mut set = Set::new(locators_data).unwrap();
    let mut now = Instant::now();
    let mut stream = set.range().ge("1-").le("10").into_stream();
    let mut elapsed = now.elapsed();
    println!("Time for retrieving a value with range(): {:.2?}", elapsed);

    let mut keys = vec![];
    while let Some(key_bytes) = stream.next() {
        keys.push(std::str::from_utf8(key_bytes).unwrap().to_string());
    }
    println!("{:?}", keys.len());

    // test for vector
    let heading_ids_to_locators_ids_data =
        fs::read(index_dir_path.clone() + "heading_ids_to_locators_ids.json").unwrap();
    let heading_ids_to_locators_ids: Vec<String> = serde_json_wasm::de::from_str(
        std::str::from_utf8(&heading_ids_to_locators_ids_data).unwrap(),
    )
    .expect("err: unable to parse json");

    let now = Instant::now();
    let locators = heading_ids_to_locators_ids.get(1).unwrap();
    let elapsed = now.elapsed();
    println!("Time for retrieving a value with vector: {:.2?}", elapsed);
    let locators_splitted: Vec<&str> = locators.split("␟").collect();
    println!("{:?}", locators_splitted.len());

    let mut new_locators: Vec<String> = Vec::new();
    for (i, value) in heading_ids_to_locators_ids.iter().enumerate() {
        let heading_ids: Vec<&str> = value.split("␟").collect();

        for heading_id in heading_ids.iter() {
            let new_value = format!("{}-{}", i, heading_id);
            new_locators.push(new_value);
        }
    }
    new_locators.sort();

    let fst_file_handle =
        std::fs::File::create("new-locators.fst").expect("Cannot create the FST file.");
    let fst_buffered_writer = io::BufWriter::new(fst_file_handle);

    let mut fst_builder = SetBuilder::new(fst_buffered_writer).expect("Generate map builder.");

    for value in new_locators.iter() {
        fst_builder.insert(value).unwrap();
    }

    fst_builder
        .finish()
        .expect("Finish generation of the FST map.");
}

#[test]
fn test_1() {
    use std::fs;
    use std::time::Instant;
    use flate2::read::GzDecoder;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/lemmata/".to_string();

    let locators_data = fs::read(index_dir_path.clone() + "subjects_section.fst").unwrap();
    let headings_data = fs::read(index_dir_path.clone() + "objects_section.fst").unwrap();
    let heading_ids_to_locators_ids_data =
        fs::read(index_dir_path.clone() + "heading_ids_to_locators_ids.json").unwrap();

    let mut index_search_engine = IndexSearchEngine2::new(
        locators_data,
        headings_data,
        heading_ids_to_locators_ids_data,
    );

    let heading = ".*a";
    let now = Instant::now();
    let heading_string = format!(r"(?i){}", heading);
    let headings = headings_by_heading_regex(&mut index_search_engine, heading_string.as_str());
    let elapsed = now.elapsed();
    println!(
        "=== Elapsed headings_by_heading_regex() search: {:.2?}",
        elapsed
    );
    println!("number of headings found: {:?}", headings.len());

    let heading_id = headings.values().next().unwrap();

    let now = Instant::now();
    let results = locators_by_heading_id(&mut index_search_engine, heading_id);
    let elapsed = now.elapsed();
    println!(
        "=== Elapsed locators_by_heading_id() search: {:.2?}",
        elapsed
    );
    println!("{:?}", results);
}

#[test]
fn test_locators() {
    use std::fs;
    use std::time::Instant;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/lemmata-2/".to_string();

    let locators_data = fs::read(index_dir_path.clone() + "subjects_section.fst").unwrap();
    let locators = fst::Map::new(locators_data).unwrap();

    let locator_id = 474u64;

    let now = Instant::now();
    let locator_raw = &mut locators.as_fst().get_key(locator_id).unwrap();
    let locator = std::str::from_utf8(locator_raw).unwrap();
    let elapsed = now.elapsed();

    println!("Locator search: {:.2?}", elapsed);
    println!("{:?}", locator);
}

#[test]
fn test_headings() {
    use std::fs;
    use std::time::Instant;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/lemmata/".to_string();

    let subjects_data = fs::read(index_dir_path.clone() + "subjects_section.fst").unwrap();
    let objects_data = fs::read(index_dir_path.clone() + "objects_section.fst").unwrap();
    let heading_ids_to_locators_ids_data =
        fs::read(index_dir_path.clone() + "heading_ids_to_locators_ids.json").unwrap();

    let mut index_search_engine = IndexSearchEngine::new(
        subjects_data,
        objects_data,
        heading_ids_to_locators_ids_data,
    );

    let heading = "a.*b.*a";
    let now = Instant::now();
    let heading_string = format!(r"(?i){}", heading);
    let headings = headings_by_heading_regex(&mut index_search_engine, heading_string.as_str());
    let elapsed = now.elapsed();
    println!(
        "=== Elapsed headings_by_heading_regex() search: {:.2?}",
        elapsed
    );
    println!("number of headings found: {:?}", headings.len());
}

#[test]
fn parse_heading_ids_to_locators_ids() {
    use std::fs;
    use std::time::Instant;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/lemmata-2/".to_string();

    let heading_ids_to_locators_ids_data =
        fs::read(index_dir_path.clone() + "heading_ids_to_locators_ids.json").unwrap();

    let now = Instant::now();
    let heading_ids_to_locators_ids: Vec<String> = serde_json_wasm::de::from_str(
        std::str::from_utf8(&heading_ids_to_locators_ids_data).unwrap(),
    )
    .expect("err: unable to parse json");
    let elapsed = now.elapsed();
    println!(
        "=== Elapsed serde_json_wasm::de::from_str() search: {:.2?}",
        elapsed
    );

    //println!("{:?}", heading_ids_to_locators_ids);
}


#[test]
fn intersection_of_ranges_test() {
    use fst::automaton::Str;
    use fst::{set, IntoStreamer, Set};
    use memmap::Mmap;
    use std::fs::File;
    use std::time::Instant;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/_aggregated/".to_string();

    // test for FST range
    let locators_data = unsafe {
        Mmap::map(&File::open(index_dir_path.clone() + "new-locators.fst").unwrap()).unwrap()
    };
    let set = Set::new(locators_data).unwrap();

    // get the first stream
    let mut now = Instant::now();
    let query_string_1 = Str::new("1-").starts_with();
    let mut stream_1 = set.search(query_string_1).into_stream();
    let mut elapsed = now.elapsed();
    println!("Time for retrieving the first stream: {:.2?}", elapsed);
    /*let mut keys_1 = vec![];
    while let Some(key_bytes) = stream_1.next() {
        keys_1.push(std::str::from_utf8(key_bytes).unwrap().to_string());
    }
    println!("{:?}", keys_1.len());*/

    // get the second stream
    now = Instant::now();
    let query_string_2 = Str::new("2-").starts_with();
    let mut stream_2 = set.search(query_string_2).into_stream();
    elapsed = now.elapsed();
    println!("Time for retrieving the second stream: {:.2?}", elapsed);
    /*let mut keys_2 = vec![];
    while let Some(key_bytes) = stream_2.next() {
        keys_2.push(std::str::from_utf8(key_bytes).unwrap().to_string());
    }
    println!("{:?}", keys_2.len());*/

    now = Instant::now();
    let mut result = set.op().add(stream_1).add(stream_2).intersection();
    elapsed = now.elapsed();
    println!("Time for intersection of two streams: {:.2?}", elapsed);
    let mut keys_result = vec![];
    while let Some(key_bytes) = result.next() {
        keys_result.push(std::str::from_utf8(key_bytes).unwrap().to_string());
    }
    println!("{:?}", keys_result.len());
}

#[test]
fn vec_split_test() {
    let a = "1-1008";
    let mapping = a.as_bytes();

    let mapping_splitted: Vec<_> = mapping.split(|i| *i == 45).collect();
    let locator_id = std::str::from_utf8(mapping_splitted.get(1).unwrap()).unwrap();
    println!("{}", locator_id);
}

#[test]
fn get_fst_range_test() {
    use fst::{IntoStreamer, Set, SetBuilder, Streamer};
    use memmap::Mmap;
    use std::time::Instant;
    use std::{fs, fs::File, io};

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/_aggregated/".to_string();

    // test for FST range
    let locators_data = unsafe {
        Mmap::map(&File::open(index_dir_path.clone() + "new-locators.fst").unwrap()).unwrap()
    };
    let mut set = Set::new(locators_data).unwrap();
    let mut now = Instant::now();
    let mut stream = set.range().ge("1-").le("10").into_stream();
    let mut elapsed = now.elapsed();
    println!("Time for retrieving a value with range(): {:.2?}", elapsed);

    let mut keys = vec![];
    while let Some(key_bytes) = stream.next() {
        keys.push(std::str::from_utf8(key_bytes).unwrap().to_string());
    }
    println!("{:?}", keys.len());
}

#[test]
fn generate_fst_test() {
    use fst::SetBuilder;
    use std::time::Instant;
    use std::{fs, io};

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/index-search-engine/tests/data/".to_string();

    // test for vector
    let heading_ids_to_locators_ids_data =
        fs::read(index_dir_path.clone() + "heading_ids_to_locators_ids.json").unwrap();
    let heading_ids_to_locators_ids: Vec<String> = serde_json_wasm::de::from_str(
        std::str::from_utf8(&heading_ids_to_locators_ids_data).unwrap(),
    )
    .expect("err: unable to parse json");

    let now = Instant::now();
    let locators = heading_ids_to_locators_ids.get(1).unwrap();
    let elapsed = now.elapsed();
    println!("Time for retrieving a value with vector: {:.2?}", elapsed);
    let locators_splitted: Vec<&str> = locators.split("␟").collect();
    println!("{:?}", locators_splitted.len());

    let mut new_locators: Vec<String> = Vec::new();
    for (i, value) in heading_ids_to_locators_ids.iter().enumerate() {
        let heading_ids: Vec<&str> = value.split("␟").collect();

        for heading_id in heading_ids.iter() {
            let new_value = format!("{}-{}", i, heading_id);
            new_locators.push(new_value);
        }
    }
    new_locators.sort();

    let fst_file_handle = std::fs::File::create(index_dir_path.clone() + "new-locators.fst")
        .expect("Cannot create the FST file.");
    let fst_buffered_writer = io::BufWriter::new(fst_file_handle);

    let mut fst_builder = SetBuilder::new(fst_buffered_writer).expect("Generate map builder.");

    for value in new_locators.iter() {
        fst_builder.insert(value).unwrap();
    }

    fst_builder
        .finish()
        .expect("Finish generation of the FST map.");
}

#[test]
fn headings_by_heading_regex_test() {
    use std::fs;
    use std::time::Instant;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/index-search-engine/module/tests/data/".to_string();
    let headings_data = fs::read(index_dir_path.clone() + "headings.fst").unwrap();
    let headings = fst::Map::new(headings_data).unwrap();
}

#[test]
fn mappings_as_fst_test() {
    use fst::SetBuilder;
    use serde_json;
    use std::path::Path;
    use std::{fs, io};

    let dir_path = Path::new("/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/_aggregated/");

    let fst_file_handle = std::fs::File::create(dir_path.join("heading_ids_to_locators_ids.fst"))
        .expect("Cannot create the FST file.");
    let fst_buffered_writer = io::BufWriter::new(fst_file_handle);

    let mut fst_builder = SetBuilder::new(fst_buffered_writer).expect("Generate map builder.");
    let mut array: Vec<String> = Vec::new();

    for n in 1..250017 {
        array.push(n.to_string());
    }

    array.sort();

    for n in array.iter() {
        fst_builder.insert(n.clone().to_string()).unwrap();
    }

    fst_builder
        .finish()
        .expect("Finish generation of the FST map.");

    fs::write(
        dir_path.join("heading_ids_to_locators_ids.json"),
        serde_json::to_string(&array).expect("Cannot serialize"),
    )
    .expect("cannot write the file");
}

#[test]
fn fst_intersection_test2() {
    use fst::automaton::Str;
    use fst::{Automaton, IntoStreamer, Set};
    use memmap::Mmap;
    use std::collections::HashSet;
    use std::fs::File;
    use std::time::Instant;

    let index_dir_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/index-search-engine/tests/data/".to_string();

    let locators_data = unsafe {
        Mmap::map(&File::open(index_dir_path.clone() + "new-locators.fst").unwrap()).unwrap()
    };
    let mut set = Set::new(locators_data).unwrap();

    // get the first stream
    let mut now = Instant::now();
    let query_1 = Str::new("1-").starts_with();
    let mut stream_1 = set.search(query_1).into_stream();
    let mut hashset_1: HashSet<String> = HashSet::new();
    while let Some(key) = stream_1.next() {
        let key = key.to_vec();
        let splitted_key: Vec<_> = key.split(|i| *i == 45).collect();
        let filtered_key = *splitted_key.get(1).unwrap();

        hashset_1.insert(String::from_utf8(filtered_key.to_vec()).unwrap());
    }
    let mut elapsed = now.elapsed();
    println!("Time for retrieving the first stream: {:.2?}", elapsed);

    // get the second stream
    now = Instant::now();
    let query_2 = Str::new("2-").starts_with();
    let mut stream_2 = set.search(query_2).into_stream();
    let mut hashset_2: HashSet<String> = HashSet::new();
    while let Some(key) = stream_2.next() {
        let key = key.to_vec();
        let splitted_key: Vec<_> = key.split(|i| *i == 45).collect();
        let filtered_key = *splitted_key.get(1).unwrap();

        hashset_2.insert(String::from_utf8(filtered_key.to_vec()).unwrap());
    }
    elapsed = now.elapsed();
    println!("Time for retrieving the second stream: {:.2?}", elapsed);

    // get the third stream
    now = Instant::now();
    let query_3 = Str::new("2-").starts_with();
    let stream_3 = set.search(query_3).into_stream();
    let vector_3 = stream_3.into_strs().unwrap();
    let mut hashset_3: HashSet<String> = HashSet::new();
    for key in vector_3.iter() {
        let splitted_key: Vec<_> = key.split("-").collect();
        let filtered_key = *splitted_key.get(1).unwrap();

        hashset_3.insert(filtered_key.to_string());
    }
    elapsed = now.elapsed();
    println!("Time for retrieving the third stream: {:.2?}", elapsed);

    // calculate intersection
    now = Instant::now();
    let intersection = hashset_1.intersection(&hashset_2);
    elapsed = now.elapsed();
    println!("Time for calculating the intersection: {:.2?}", elapsed);
    println!("{:?}", intersection.size_hint().1.unwrap());
}

#[test]
fn ihl_size_test() {
    use fst::SetBuilder;
    use memmap::Mmap;
    use std::{fs::File, io};

    let base_path = "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/examples/dwds-data/public/peritext/indexes/_heading_ids_to_locators_ids/".to_string();
    let mut keys: BTreeSet<String> = BTreeSet::new();

    // load the first hl file
    let hl_1_data = unsafe {
        Mmap::map(&File::open(base_path.clone() + "3/heading_ids_to_locators_ids.fst").unwrap())
            .unwrap()
    };
    let hl_1_set = Set::new(hl_1_data).unwrap();
    let mut hl_1_stream = hl_1_set.stream();
    let mut counter = 1usize;
    while let Some(key) = hl_1_stream.next() {
        let key_splitted: Vec<_> = key.split(|i| *i == SUFFIX_DELIMITER).collect();
        let heading_id = *key_splitted.get(0).unwrap();
        let locator_id = *key_splitted.get(1).unwrap();
        let new_key = format!(
            "{}{}{}{}{}{}",
            0,
            SUFFIX_DELIMITER_AS_CHAR.to_string(),
            std::string::String::from_utf8(locator_id.to_vec()).unwrap(),
            SUFFIX_DELIMITER_AS_CHAR.to_string(),
            std::string::String::from_utf8(heading_id.to_vec()).unwrap(),
            counter,
        );

        keys.insert(new_key);
        counter += 1usize;
    }
/*
    // load the second hl file
    let hl_2_data = unsafe {
        Mmap::map(&File::open(base_path.clone() + "1/heading_ids_to_locators_ids.fst").unwrap())
            .unwrap()
    };
    let hl_2_set = Set::new(hl_2_data).unwrap();
    let mut hl_2_stream = hl_2_set.stream();
    while let Some(key) = hl_2_stream.next() {
        let key_splitted: Vec<_> = key.split(|i| *i == SUFFIX_DELIMITER).collect();
        let heading_id = *key_splitted.get(0).unwrap();
        let locator_id = *key_splitted.get(1).unwrap();
        let new_key = format!(
            "{}{}{}{}{}",
            1,
            SUFFIX_DELIMITER_AS_CHAR.to_string(),
            std::string::String::from_utf8(locator_id.to_vec()).unwrap(),
            SUFFIX_DELIMITER_AS_CHAR.to_string(),
            std::string::String::from_utf8(heading_id.to_vec()).unwrap()
        );

        keys.insert(new_key);
    }

    // load the third hl file
    let hl_3_data = unsafe {
        Mmap::map(&File::open(base_path.clone() + "2/heading_ids_to_locators_ids.fst").unwrap())
            .unwrap()
    };
    let hl_3_set = Set::new(hl_3_data).unwrap();
    let mut hl_3_stream = hl_3_set.stream();
    while let Some(key) = hl_3_stream.next() {
        let key_splitted: Vec<_> = key.split(|i| *i == SUFFIX_DELIMITER).collect();
        let heading_id = *key_splitted.get(0).unwrap();
        let locator_id = *key_splitted.get(1).unwrap();
        let new_key = format!(
            "{}{}{}{}{}",
            2,
            SUFFIX_DELIMITER_AS_CHAR.to_string(),
            std::string::String::from_utf8(locator_id.to_vec()).unwrap(),
            SUFFIX_DELIMITER_AS_CHAR.to_string(),
            std::string::String::from_utf8(heading_id.to_vec()).unwrap()
        );

        keys.insert(new_key);
    }

    // load the fourth hl file
    let hl_4_data = unsafe {
        Mmap::map(&File::open(base_path.clone() + "3/heading_ids_to_locators_ids.fst").unwrap())
            .unwrap()
    };
    let hl_4_set = Set::new(hl_4_data).unwrap();
    let mut hl_4_stream = hl_4_set.stream();
    while let Some(key) = hl_4_stream.next() {
        let key_splitted: Vec<_> = key.split(|i| *i == SUFFIX_DELIMITER).collect();
        let heading_id = *key_splitted.get(0).unwrap();
        let locator_id = *key_splitted.get(1).unwrap();
        let new_key = format!(
            "{}{}{}{}{}",
            3,
            SUFFIX_DELIMITER_AS_CHAR.to_string(),
            std::string::String::from_utf8(locator_id.to_vec()).unwrap(),
            SUFFIX_DELIMITER_AS_CHAR.to_string(),
            std::string::String::from_utf8(heading_id.to_vec()).unwrap()
        );

        keys.insert(new_key);
    }
*/
    // save the file
    let fst_file_handle = std::fs::File::create("ihl.fst").expect("Cannot create the FST file.");
    let fst_buffered_writer = io::BufWriter::new(fst_file_handle);

    let mut fst_builder = SetBuilder::new(fst_buffered_writer).expect("Generate map builder.");

    for key in keys.iter() {
        fst_builder.insert(key).unwrap();
    }

    fst_builder
        .finish()
        .expect("Finish generation of the FST map.");

    //println!("{:?}", keys);
}