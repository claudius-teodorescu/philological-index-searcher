# static-search-engine


## Scope
This module is designated to be used for searching within a philological index created based upon [the Text Index Ontology](https://claudius-teodorescu.gitlab.io/text-index-ontology/index-en.html), and compressed by using the [index-as-triples-to-compressed-index](https://gitlab.com/claudius-teodorescu/index-as-triples-to-compressed-index).

## Technology stack
This module is currently written in Javascript and Rust compiled to WebAssembly. Maybe, in the future, it will be helpful to have the module be called from a Javascript service worker (with Request / Response), with the dataset located according to (File and Directory Entries API)[https://developer.mozilla.org/en-US/docs/Web/API/File_and_Directory_Entries_API], in order to simulate a server for static websites.

## Functions

These APIs designed considering the philological perspective on how the search in philological indexes.

* headings_by_heading_regex(regex_string)

* headings_by_heading_levenstein1(levenstein1_string)

* headings_by_heading_levenstein2(levenstein2_string)

* locators_by_heading(heading_id)

## To do list

* Add ```log()```, with ```loading``` and ```searching``` parameters as static fields. This function can be called from Javascript at any time, and it will return all the current log, from the start of the search engine to the current time. About the format of the log records: record to start with date/time formatted after the W3C’s profile of ISO 8601, and to use tab as the field separator.

* Set index caching method, which will help in case of very large indexes. A possible method would be to have a small file, called ```index.txt```, containing the version (as datetime) of the index. If the search engine finds that there is a new version of the index, it will download it.

* Silent error message when there are no associated indexes in calling ```associated_headings_by_locators()```.

* Create computed indexes in browser. Computed index: locator-all_heading_ids (record example: 108-1_2-17-40_77_108).

* Function locators-and-other-headings-by-heading-id(index-position, heading-id, index-positions)?

* Check if it is possible to make ngram queries with regular expressions (see some of the ```Resources for ngram search``` from below).

* If ```rdfstore-js``` is lighter and faster than ```oxigraph```, use it instead of the latter.

* Ideas for functions:

    * triple-pattern-fragment(triple-pattern)

    * This function is to be called as part of an execution plan for a SPARQL query.

    * locate_by_exact_form("subjects|predicates|objects", value)

    * locate_by_regex("subjects|predicates|objects", value)

    * locate_by_prefix("subjects|predicates|objects", value)

    * locate_by_ngram("subjects|predicates|objects", value)

    * extract("subjects|predicates|objects", id)

    * translate(triple-pattern-fragment)

    * get-all-subjects/predicates/objects()

    * get-subjects/predicates/objects()

    * For the last ones, the arguments can also be functions for search (filtering?).

    * Function headings-by-locator-id(index-position, locator-id)?

    * Function intersection_by_suffix_with_maximum_offset(suffix_delimiter, offset)?

    * Function intersection_by_suffix_with_exact_offset(suffix_delimiter, offset)?

    * Function intersection_with_meximum_offset(offset)?

    * Function intersection_with_exact_offset(offset)?

    * Function into_suffixes(suffix_delimiter: &str)?

    * Function into_suffixes_as_u64(suffix_delimiter: &str)?

    * exact("term")

    * prefix("prefix")

    * regex("regex")

    * ngram(?object, "token")

    * o-max("token_1", "token_2", 3), for proximity search: returns the triples containing tokens located in a document at a maximum distance of three tokens between them, in the order of the function's arguments.

    * u-max("token_1", "token_2", 3), for proximity search: returns the triples containing tokens located in a document at a maximum distance of three tokens between them, in any order.

    * o-exact("token_1", "token_2", 3), for proximity search: returns the triples containing tokens located in a document at an exact distance of three tokens between them, in the order of the function's arguments.

    * u-max("token_1", "token_2", 3), for proximity search: returns the triples containing tokens located in a document at an exact distance of three tokens between them, in any order.

* For the future: Functions allowed in triple pattern, in any position.

* NEEDED? Add the function fst::set::Stream::into_suffixes(suffix_delimiter: &str).

* What about generating the (i)hl data structure from (i)lhp data structure, when the static search engine is initialized? Or, maybe generate the one from the other one that is smaller.

* Ideas for functions: https://github.com/doriantaylor/xslt-rdfa?tab=readme-ov-file#programming-interface

## Resources for ngram search

* [Index based on positional trigrams](https://github.com/google/zoekt/blob/master/doc/faq.md#how-much-resources-does-zoekt-require)

* [Processing SPARQL queries with regular expressions in RDF databases](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-12-S2-S6)

## Resources

* [OSCAR: A customisable tool for free-text search over SPARQL endpoints](https://save-sd.github.io/2018/accepted/heibi/index.html)

* [rdfstore-js](https://github.com/antoniogarrote/rdfstore-js)

* [Foundations of Traversal Based Query Execution over Linked Data (Extended Version)](https://arxiv.org/abs/1108.6328)

* [Visual SPARQL Builder](http://leipert.github.io/vsb/dbpedia/#/workspace)

* [Rust to WebAssembly the hard way](https://surma.dev/things/rust-to-webassembly/)

* [Crate serde_wasm_bindgen](https://docs.rs/serde-wasm-bindgen/latest/serde_wasm_bindgen/)

* [Avoiding using Serde in Rust WebAssembly When Performance Matters](https://medium.com/@wl1508/avoiding-using-serde-and-deserde-in-rust-webassembly-c1e4640970ca)

* [GitHub - crabnebula-dev/fst-no-std](https://github.com/crabnebula-dev/fst-no-std)