import jsUntar from "https://cdn.jsdelivr.net/npm/js-untar@2.0.0/+esm";
import StaticSearchEngine from "../public/index.js";

const static_search_engine = new StaticSearchEngine();
document.addEventListener("static-search-engine:indexes-archive-decompressing-time", (event) => {
    document.querySelector("output#indexes-archive-decompressing-time").value = event.detail;
});
document.addEventListener("static-search-engine:indexes-archive-extraction-time", (event) => {
    document.querySelector("output#indexes-archive-extraction-time").value = event.detail;
});
document.addEventListener("static-search-engine:indexes-metadata-loading-time", (event) => {
    document.querySelector("output#indexes-metadata-loading-time").value = event.detail;
});
document.addEventListener("static-search-engine:indexes-loading-time", (event) => {
    document.querySelector("output#indexes-loading-time").value = event.detail;
});

//await static_search_engine.init("https://localhost:8080/index.tar.gz");
await static_search_engine.init("https://claudius-teodorescu.gitlab.io/gretil-corpus-data/peritext/indexes/words-fulltext/index.tar.gz");

let index_searcher = static_search_engine.searcher;
/*
    lexical-categories / b.*
    bibl-dates / 1.*
    headwords / 4.*
    grammatical-categories / a.*
*/

let iterations_number = 1;
let index_position = "0";
let heading_string = "fr.*";

let start = performance.now();
let headings_map = {};
for (let step = 0; step < iterations_number; step++) {
    headings_map = index_searcher.headings_by_heading_regex(index_position, heading_string);
}
let end = performance.now();
document.querySelector("output#headings-by-heading-regex-time").value = (end - start) / iterations_number;
console.log(headings_map);

let first_heading_id = headings_map.values().next().value;

console.log(first_heading_id);

let locators_map = {};
let locators = [];
if (first_heading_id !== undefined) {
    iterations_number = 1;
    start = performance.now();
    for (let step = 0; step < iterations_number; step++) {
        locators_map = index_searcher.locators_by_heading_id(index_position, first_heading_id);
        locators = Array.from(locators_map.keys());
    }
    end = performance.now();
    document.querySelector("output#locators-by-heading-id-time").value = (end - start) / iterations_number;

    console.log(locators.length);
    console.log(locators);
}
console.log(locators_map);

// test associated_headings_by_locators()
/*iterations_number = 1;
let locators_map_tmp = Array.from(locators_map).slice(0, 29);
let locators_sliced = new Map(locators_map_tmp);

start = performance.now();
let associated_headings = {};
for (let step = 0; step < iterations_number; step++) {
    associated_headings = index_searcher.associated_headings_by_locators(locators_sliced, ["0", "2"]);
}
end = performance.now();
document.querySelector("output#associated_headings_by_locators-time").value = (end - start) / iterations_number;
console.log(Object.keys(associated_headings).length);
console.log(associated_headings);*/

// 6ms / 0ms / 120ms
// 29ms / 0.023ms / 0.003ms
